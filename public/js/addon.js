
   AJS.$('#dialogCheckButton').click(function(){

       AJS.$('#token').html('<b>Waiting for token after click....dialog should be closed and issue view refreshed.</b>');
        AP.context.getToken(function(token){
            console.log('Token', token);
            AJS.$('#token').html(token);
            AP.dialog.close();
            AP.jira.refreshIssuePage();
        });


    });


   AJS.$('#openDialogButton').click(function(){
       AP.dialog.create({
           key: 'dialog-key',
           size: 'x-large',
           chrome: false,
           closeOnEscape: false
       });

   });

