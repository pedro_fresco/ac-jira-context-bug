export default function routes(app, addon) {
    app.get('/', (req, res) => {
        res.redirect('/atlassian-connect.json');
    });

    app.get('/dialog', addon.authenticate(), (req, res) => {

        res.render('dialog', {
            title: 'Atlassian Connect'
        });
    });

    app.get('/issue-content', addon.authenticate(), (req, res) => {
        res.render('issue-view', {
            title: 'Enjoy'
        });
    });

}
